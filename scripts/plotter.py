import sys
import os

sys.path.insert(0,"/usr/lib/kicad-nightly/lib/python3/dist-packages/")

import pcbnew
from pcbnew import *

file_name = os.path.abspath(sys.argv[1])
output_dir = os.path.abspath(sys.argv[2])

print("Running KiCAD Plotter CI/CD Script on %s output to %s"%(file_name, output_dir,))

try:
    os.makedirs(output_dir)
except OSError:
    pass


board = pcbnew.LoadBoard(file_name)
pctl = pcbnew.PLOT_CONTROLLER(board)
popt = pctl.GetPlotOptions()
popt.SetOutputDirectory(output_dir)
popt.SetPlotFrameRef(False)
#popt.SetLineWidth(pcbnew.FromMM(0.1))

popt.SetAutoScale(False)
popt.SetScale(1)
popt.SetMirror(False)

popt.SetUseGerberAttributes(True)
popt.SetUseGerberProtelExtensions(False)

popt.SetExcludeEdgeLayer(True)
popt.SetUseAuxOrigin(False)
pctl.SetColorMode(True)

popt.SetSubtractMaskFromSilk(False)
popt.SetPlotReference(True)
popt.SetPlotValue(False)

popt.SetDrillMarksType(0)

layers = [
    ("F.Cu", pcbnew.F_Cu, "Top layer"),
    ("In1.Cu", pcbnew.In1_Cu, "In1"),
    ("In2.Cu", pcbnew.In2_Cu, "In1"),
    ("B.Cu", pcbnew.B_Cu, "Bottom layer"),
    ("F.Paste", pcbnew.F_Paste, "Paste top"),
    ("B.Paste", pcbnew.B_Paste, "Paste bottom"),
    ("F.SilkS", pcbnew.F_SilkS, "Silk top"),
    ("B.SilkS", pcbnew.B_SilkS, "Silk top"),
    ("F.Mask", pcbnew.F_Mask, "Mask top"),
    ("B.Mask", pcbnew.B_Mask, "Mask bottom"),
    ("Edge.Cuts", pcbnew.Edge_Cuts, "Edges"),
]

for layer_info in layers:
    pctl.SetLayer(layer_info[1])
    pctl.OpenPlotfile(layer_info[0], pcbnew.PLOT_FORMAT_GERBER, layer_info[2])
    pctl.PlotLayer()
    pctl.ClosePlot()


drlwriter = EXCELLON_WRITER( board )
drlwriter.SetMapFileFormat( PLOT_FORMAT_PDF )

mirror = False
minimalHeader = False
offset = wxPoint(0,0)
# False to generate 2 separate drill files (one for plated holes, one for non plated holes)
# True to generate only one drill file
mergeNPTH = False
drlwriter.SetOptions( mirror, minimalHeader, offset, mergeNPTH )

metricFmt = True
drlwriter.SetFormat( metricFmt )

genDrl = True
genMap = True
drlwriter.CreateDrillandMapFilesSet( pctl.GetPlotDirName(), genDrl, genMap );

# One can create a text file to report drill statistics
rptfn = pctl.GetPlotDirName() + 'drill_report.rpt'
drlwriter.GenDrillReportFile( rptfn );