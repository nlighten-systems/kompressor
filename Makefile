.PHONY: bom clean docker_build docker_run build output/version.txt board_render.png board_render_purple.png

GIT_VERSION := $(shell git describe --tags)
PYTHON := python3
OUTPUT_DIR := output
OUTPUT_FILE := $(OUTPUT_DIR)/kompressor-$(GIT_VERSION).zip
LOGFILE?=$(OUTPUT_DIR)/kibot_error.log

all: build

clean:
	rm -rf output

docker_build:
	cd dockerbuild && docker build -t kicad_ci . 
	docker tag kicad_ci registry.gitlab.com/nlighten-systems/kompressor

pushdockerimage: dockerbuild/Dockerfile
	docker login registry.gitlab.com
	cd dockerbuild && docker build -t registry.gitlab.com/nlighten-systems/kompressor .
	docker push registry.gitlab.com/nlighten-systems/kompressor

docker_run:
	docker run --rm -it -u "$(shell id -u):$(shell id -g)" -v "$(abspath .):/data" kicad_ci /bin/bash

output:
	mkdir output

output/version.txt: | output
	echo "$(GIT_VERSION)" > $@

build: output/version.txt output/.panel_build | output
	cp README.md output/
	cp LICENSE.txt output/
	kibot -g variant=915Mhz -d output -c kompressor/variants.kibot.yaml -e kompressor/kompressor.kicad_sch 2>> $(LOGFILE)
	kibot -g variant=868Mhz -d output -c kompressor/variants.kibot.yaml -e kompressor/kompressor.kicad_sch 2>> $(LOGFILE)
	kibot -d output -c kompressor/common.kibot.yaml -e kompressor/kompressor.kicad_sch 2>> $(LOGFILE)


board_render.png:
	kibot -g variant=915Mhz -d output/915 -c kompressor/kompressor.kibot.yaml -e kompressor/kompressor.kicad_sch -s all "basic_render_3d_30deg"
	cp output/915/3D/kompressor-3D_top30deg_915Mhz.png board_render.png

board_render_purple.png:
	kibot -d output -c kompressor/common.kibot.yaml -e kompressor/kompressor.kicad_sch -s all "basic_pcbdraw_png_purple_bottom"
	cp output/PCB/2D_render/purple/kompressor-bottom.png board_render_purple.png

output/kompressor-panel.kicad_pcb: kompressor/kompressor.kicad_pcb | output
	kibot -d output -c kompressor/common.kibot.yaml -e kompressor/kompressor.kicad_sch -s all 'panel' 2> $(LOGFILE)

output/.panel_build: output/kompressor-panel.kicad_pcb
	kibot -d output/panel -c kompressor/panel.kibot.yaml -b output/kompressor-panel.kicad_pcb 2> $(LOGFILE)
	touch $@

