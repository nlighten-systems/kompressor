# Project Kompressor

Reference implementation of a [Helium HIP 72](https://github.com/helium/HIP/blob/main/0072-secure-concentrators.md) LoRaWAN Secure Concentrator card designed by [NLighten Systems](https://nlighten-systems.com) with help from Helium Foundation Grant.

![image](rf_shield/render.png)
![image](board_render.png)

This hardware design is licensed open-source under the CERN-OHL-W v2 License. See LICENSE.txt. It has been certified to meet the definition of Open Source Hardware by oshwa.org. `[OSHW] US002135`

![image](oshw-cert.svg)

![image](https://github.com/helium/HIP/raw/main/secure-concentrators/hardware_block.png)

### Note on the SX1261

The Semtech SX1303 reference design includes a SX1261 radio used for listen-before-talk feature in addition to the SX1303 + 2x SX1250. The SX1261 is NOT currently used in this design. 

## GPS

The GPS receiver is required component. This project uses ZOE-M8Q. In the future we would like to move to the newer U-Blox MIA-M10Q.

## Microcontroller

The microcontroller must have the ability to securely store a unique crypto key and prevent readout. Any STM32 can do this by disabling the jtag port. In general, we would prefer a newer type ARM core (M33) because we may someday make use of its security features. We also prefer faster clock speed because that has a direct relation to the number of signed packets/second it can handle. The other requirements for this application are:

 * 256KiB+ Flash
 * 20KiB+ SRAM
 * 2x SPI
 * 2x UART
 * 1x USB (device)


| MPN          | CPU         | Flash  | SRAM  |
|--------------|-------------|--------|-------|
| GD32E503CET  | 180Mhz M33  | 512kb  | 128kb |
| STM32U575CGT | 160 Mhz M33 | 1024kb | 786kb |
| STM32U575CIT | 160 Mhz M33 | 2048kb | 786kb |
| STM32U585CIT | 160 Mhz M33 | 2048kb | 786kb |
| STM32L4P5CET | 120 Mhz M4  | 512kb  | 320kb |
| STM32L4P5CGT | 120 Mhz M4  | 1024kb | 320kb |
| STM32L4Q5CGT | 120 Mhz M4  | 1024kb | 320kb |
| STM32L552CCT | 110Mhz M33  | 256kb  | 256kb |
| STM32L552CET | 110Mhz M33  | 512kb  | 256kb |
| STM32L562CET | 110Mhz M33  | 512kb  | 256kb |
| STM32L433CCT | 80Mhz M4    | 256kb  | 64kb  |
| STM32L443CCT | 80Mhz M4    | 256kb  | 64kb  |
| STM32L452CET | 80Mhz M4    | 512kb  | 160kb |

## SAW Filter

 * 903-928Mhz
 * 50ohm input/output

 [Digikey Search](https://www.digikey.com/en/products/filter/saw-filters/836?s=N4IgjCBcoGwJxVAYygMwIYBsDOBTANCAPZQDaIALAAxwDMdIAuoQA4AuUIAymwE4CWAOwDmIAL6F6ADkQgUkDDgLEy4GGABMtCMxDtOPASPGEY1WfMV5CJSOVpUqtLU1YdI3PkNESQAWg0LKD4AV2VbcgBWEEIERjEEoA)

B39921B4301F210

## 1.2v Switch Regulator

 * 1.2v fixed
 * +1.5Mhz switching freq

[Digikey Search](https://www.digikey.com/en/products/filter/pmic-voltage-regulators-dc-dc-switching-regulators/739?s=N4IgjCBcoCwdIDGUBmBDANgZwKYBoQB7KAbRAGYYB2GciAugBnKvJAF0CAHAFyhADKPAE4BLAHYBzEAF8GADhhRQySOmz4ipcADZ5lAEwgCYA3oNKTZsAE5Gx8AfkBWPQ7DlGjKvPd153g4GjAZg8jruVAY2ihzcfJCCIhLScuCMMcpIqJi4BMSQZOQG5DpgVHEgvPxCYlKyJlRUNlmq6nlahSAw8lSM9N3yNjDODj3DFQTjOvZTQzpGczY6o0s2bFN2XmNbEZteLZxVCUl1qQQAtEYIqiIArpoFZKsghzJpEQiiACb8F2D9BzVRIOHgATy4OH4aCwyHeQA)

 * TPS622319DRYT
 * TPS62235DRYT
 * TPS62242

## Load Switch

 * Switch on 3.3 with 1.2v

 [Digikey Search](https://www.digikey.com/en/products/filter/pmic-power-distribution-switches-load-drivers/726?s=N4IgjCBcoCwdIDGUBmBDANgZwKYBoQB7KAbRAGYAOAVjAAZyQBdAgBwBcoQBldgJwCWAOwDmIAL4Ew1GFFDJI6bPiKkKlMAE4AbNRAFy27QHYGzNp0g9%2BwsZJABaAExykUfgFcVxSGT0FNZnFgoA)

 * TPS22908
 * TPS22915
 * TPS22930
 * AP22913
 * TPS22967
 * MIC94071YC6-TR


 ## TCXO


 * 32Mhz
 
 [DigiKey Search](https://www.digikey.com/en/products/filter/oscillators/172?s=N4IgjCBcoGwJxVAYygMwIYBsDOBTANCAPZQDaIALAAxwDMdIAuoQA4AuUIAymwE4CWAOwDmIAL6EwADlqIQKSBhwFiZSmCowATDCasOkbnyGiJILWACsVOe04BVQfzYB5VAFlc6bAFdeuEEIfTncACQAvQJAAWyFOWi0o6PQAD3jEswBaROh5KD4fFRJIcksohEYxM11c-gATTkyNCH1OKLYATxYAw28UKqA)

 ## TVS Diode (D6)

  * ESD protection for +3v3 rail
  * 0201 size

[DigiKey Search](https://www.digikey.com/en/products/filter/tvs-diodes/144?s=N4IgjCBcpgbFoDGUBmBDANgZwKYBoQB7KAbRAGYBOABgHYAOEAXQIAcAXKEAZXYCcAlgDsA5iAC%2BBWJQQhkkdNnxFSIACzVKVcszadIPfsLGSQtWBGhzUmXAWKQyYWmrAAmNyALPXYAKy6IBxcvIKiEgQAtJ5W8vwArsoOZAEEMkzipvBWAgAmXJFg1BB6XF4gAI7sAJ5cJSA1rDhcaFjImUA)
