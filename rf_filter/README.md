The RF front end has a low-pass filter made of discrete components. The purpose is to block unwanted harmonics. From looking at the Semtech reference designa and the SKY66423-11 datasheet, it looks like the Semtech engineers took the RF filter design from the SKY66423 and tweeked it slightly. The RF filter seems to be a 4th order Ellptic with a cut-off frequency of the second harmonic. However, they seem to have made a mistake an forgotten to populate the first shunt capacitor.

https://incompliancemag.com/article/s-parameters-and-emi-filters-response/

## 868Mhz

The SKY66423-11 datasheet has an example RF filter:

```
     +-- 1.5pF -+
     |          |
FEM--+- 5.6nH --+ - 5.6nH-+--56pF--ANT
     |          |         |
     NC       2.2pF       NC
     |          |         |
```

This simulated response in LTSpice makes it seem like the original designers are trying to target the second harmonic directly. The response curve looks like a 4th order elliptic filter.

### 915Mhz

The SKY66423-11 Datasheet has an example RF filter to use for 915Mhz that looks like this:

```
     +-- NC ----+
     |          |
FEM--+- 4.7nH --+ - 4.7nH-+--56pF--ANT
     |          |         |
     NC       1.8pF       NC
     |          |         |
```

Note the the lack of parallel cap. I think this is a mistake. The cap is populated in the Semtech reference design.

The 915Mhz reference design from Semtech:


```
     + - 2.1pF -+
     |          |
FEM--+- 3.3nH --+ - 4.7nH-+--56pF--ANT
     |          |         |
     NC       1.8pF       NC
     |          |         |
```

Semtech seems to have corrected the mistake from the SKY66423 reference design by adding the parallel cap. However, when simulating with LTSpice, this filter is still not great a blocking the 2nd harmonic (1.830Ghz) with -12.5dB. 

The 915Mhz RAK design:

```
     + - 1.5pF -+
     |          |
FEM--+- 4.7nH --+ - 4.7nH-+--56pF--ANT
     |          |         |
     NC       2.2pF       NC
     |          |         |
```

RAK's design is better with blocking 2nd harmonic with -19.6dB.
